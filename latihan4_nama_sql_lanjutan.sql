--create table Classbe
CREATE TABLE Classbe(
	Id INT,
	Nama VARCHAR(30),
	Tema VARCHAR(30),
	Totalpertemuan INT,
	PRIMARY KEY (Id)
);

--create table kelas
CREATE TABLE kelas(
	Id INT,
	Id_be INT,
	Date DATE,
	Pertemuanke INT,
	PRIMARY KEY (Id),
	FOREIGN KEY (Id_be) REFERENCES Classbe(Id)
);

--insert data to table Classbe
INSERT INTO Classbe
  (Id, Nama, Tema, Totalpertemuan)
VALUES
  (1, 'a', 'tipe data', 3), 
  (2, 'b', 'method', 2), 
  (3, 'c', 'oop inheritance', 3),
  (4, 'd', 'oop encapsulasi', 2),
  (5, 'e', 'oop abrak', 5);
  
--insert data to table kelas
INSERT INTO kelas
  (Id, Id_be, Date, Pertemuanke)
VALUES
  (1, 1, '2019-10-10', 1), 
  (2, 1, '2019-09-10', 2), 
  (3, 1, '2019-08-10', 3),
  (4, 2, '2019-07-10', 1),
  (5, 3, '2019-06-10', 1);
  
--select semua data dari table Classbe
SELECT *
FROM Classbe;

--select semua data dari table kelas
SELECT *
FROM kelas;

--Tampilkan data yang memiliki tanggal pertemuan > 2019-09-10′  menggunakan Inner Join
SELECT *
FROM Classbe a
INNER JOIN kelas b
ON a.Id = b.Id_be
WHERE Date > '2019-09-10';

--Tampilkan data  menggunakan left Join
SELECT *
FROM Classbe a
LEFT JOIN kelas b
ON a.Id = b.Id_be;

--Tampilkan data  menggunakan  right Join
SELECT *
FROM Classbe a
RIGHT JOIN kelas b
ON a.Id = b.Id_be;

-- Tampilkan data menggunakan full Join
SELECT *
FROM Classbe a
FULL JOIN kelas b
ON a.Id = b.Id_be;

-- Tampilkan data classbe dimana tema diawali huruf o dan huruf kelimanya adalah i.
SELECT *
FROM Classbe
WHERE Tema LIKE 'o___i%';